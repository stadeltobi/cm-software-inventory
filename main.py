#!/usr/bin/env python3
import argparse
import json
import logging
import os
import re
import subprocess
from datetime import date
from subprocess import PIPE

APT_DESCRIPTION_LINE_PREFIX = 'Description: '
APT_PACKAGE_LINE_PRFIX = 'Package: '
APT_VERSION_LINE_PREFIX = 'Version: '

PIP_SUMMARY_LINE_PREFIX = 'Summary: '
PIP_NAME_LINE_PRFIX = 'Name: '
PIP_VERSION_LINE_PREFIX = 'Version: '


class Entry:
    source: str   # the collector that created this entry
    hostname: str  # the hostname of the machine this entry was collected from
    architecture: str  # the architecture of the machine this entry was collected from
    package: str  # the name of the package
    version: str  # the version of the package
    description: str  # a short description of the package (optional)
    license: str  # whether the package is free to use or paid
    is_managed: bool  # is this package managed by the package manager?
    captured: str  # the date this entry was captured

    def __init__(self):
        self.captured = date.today().isoformat()

    def __str__(self):
        return f'{self.package} ({self.description}, version {self.version})'


def apt_collector(hostname, arch, **kwargs):
    logging.info('*** Collecting apt packages ***')
    apt_packages = get_manually_installed_apt_packages()

    result = []

    out, err = subprocess.Popen(['apt', 'show'] + apt_packages, stdout=PIPE, stderr=PIPE).communicate()
    entry = None
    for line in out.decode('utf-8').split('\n'):
        if line.startswith(APT_PACKAGE_LINE_PRFIX):
            entry = Entry()
            entry.package = line[len(APT_PACKAGE_LINE_PRFIX):]
            entry.source = 'apt'
            entry.license = 'free'
            entry.hostname = hostname
            entry.architecture = arch
            entry.is_managed = True
            result.append(entry)
            logging.info(f'{entry.source} is adding entry {entry.package} to inventory')
        elif line.startswith(APT_VERSION_LINE_PREFIX):
            entry.version = line[len(APT_VERSION_LINE_PREFIX):]
        elif line.startswith(APT_DESCRIPTION_LINE_PREFIX):
            entry.description = line[len(APT_DESCRIPTION_LINE_PREFIX):]

    return result


def get_manually_installed_apt_packages():
    out, err = subprocess.Popen(['apt-mark', 'showmanual'], stdout=PIPE, stderr=PIPE).communicate()
    packages = out.decode('utf-8').split('\n')
    return packages


def compgen_collector(hostname, arch, **kwargs):
    logging.info('*** Collecting binaries in PATH via compgen ***')
    everything, err = subprocess.Popen(['bash', '-c', 'compgen -c | sort | uniq'],
                                       stdout=PIPE, stderr=PIPE).communicate()
    everything = everything.decode('utf-8').split('\n')
    builtins, err = subprocess.Popen(['bash', '-c', 'compgen -abfk | sort | uniq'],
                                     stdout=PIPE, stderr=PIPE).communicate()
    builtins = builtins.decode('utf-8').split('\n')
    # remove builtins from everything if existing
    for b in builtins:
        if b in everything:
            everything.remove(b)

    result = []
    for program in everything:
        entry = Entry()
        entry.source = 'compgen'
        entry.hostname = hostname
        entry.package = program
        entry.version = 'unknown'
        entry.architecture = arch
        entry.license = 'free'
        entry.description = 'unknown'
        entry.is_managed = True
        logging.info(f'{entry.source} is adding entry {entry} to inventory')
        result.append(entry)
    return result


def npm_collector(hostname, _, **kwargs):
    logging.info('*** Collecting npm packages ***')
    npm_bin = os.getenv('NVM_BIN')
    out, err = subprocess.Popen([f'{npm_bin}/npm', 'list', '--parseable', '--long'],
                                stdout=PIPE, stderr=PIPE).communicate()
    out = out.decode('utf-8').split('\n')[1:]
    result = []
    for i in out:
        entry = Entry()
        entry.source = 'npm'
        entry.hostname = hostname
        line = i.split(':')
        remainder = line[1]
        package_splitpoint = 1 if remainder.startswith('@') else 0
        entry.package = remainder.split('@')[package_splitpoint]
        if package_splitpoint == 1:
            entry.package = f'@{entry.package}'
        entry.version = remainder.split('@')[package_splitpoint + 1].split(':')[0]
        entry.architecture = 'all'
        entry.license = 'free'
        entry.description = 'unknown'
        entry.is_managed = True
        logging.info(f'{entry.source} is adding entry {entry} to inventory')
        result.append(entry)


def snap_collector(hostname, arch, **kwargs):
    logging.info('*** Collecting snap packages ***')
    out, err = subprocess.Popen(['snap', 'list', '--all'],
                                stdout=PIPE, stderr=PIPE).communicate()
    lines = out.decode('utf-8').split('\n')
    headers = re.findall(r'[a-z]+(?: +|$)', lines[0].lower())

    result = []
    for line in lines[1:]:
        entry = Entry()
        entry.source = 'snap'
        entry.hostname = hostname
        fields = parse_fields(headers, line)

        package = fields['name']
        if package:
            entry.package = package
            entry.version = fields['version']
            entry.architecture = arch
            entry.license = 'free'
            entry.hostname = hostname
            entry.description = 'unknown'
            entry.is_managed = True
            logging.info(f'{entry.source} is adding entry {entry} to inventory')
            result.append(entry)

    return result


def pip_collector(hostname, arch, **kwargs):
    logging.info('*** Collecting pip packages ***')
    pip_packages = get_pip_packages()

    result = []

    out, err = subprocess.Popen(['pip', 'show'] + pip_packages, stdout=PIPE, stderr=PIPE).communicate()
    entry = None
    for line in out.decode('utf-8').split('\n'):
        if line.startswith(PIP_NAME_LINE_PRFIX):
            entry = Entry()
            entry.package = line[len(PIP_NAME_LINE_PRFIX):]
            entry.source = 'pip'
            entry.license = 'free'
            entry.hostname = hostname
            entry.arch = arch
            entry.is_managed = True
            result.append(entry)
            logging.info(f'{entry.source} is adding entry {entry.package} to inventory')
        elif line.startswith(PIP_VERSION_LINE_PREFIX):
            entry.version = line[len(PIP_VERSION_LINE_PREFIX):]
        elif line.startswith(PIP_SUMMARY_LINE_PREFIX):
            entry.description = line[len(PIP_SUMMARY_LINE_PREFIX):]

    return result


def get_pip_packages():
    out, err = subprocess.Popen(['pip', 'list'],
                                stdout=PIPE, stderr=PIPE).communicate()
    lines = out.decode('utf-8').split('\n')
    if len(lines) < 2:
        raise RuntimeError(f'cannot parse pip output:\n{lines}')
    if not lines[0].startswith('Package') or not lines[1].startswith('------'):
        raise RuntimeError(f'cannot parse pip output:\n{lines[0]}\n{lines[1]}')

    packages = [line.split(' ')[0] for line in lines[2:] if line]
    return packages


def os_collector(hostname, arch, **kwargs):
    logging.info('*** Collecting OS information ***')
    with open('/etc/os-release', 'r') as file:
        data = file.read()

    logging.error(data)

    entry = Entry()
    entry.source = 'os'
    entry.hostname = hostname
    entry.package = re.search(r'(?:^|\n)ID="?([^"\n]+)"?(?:$|\n)', data).group(1)
    entry.version = re.search(r'(?:^|\n)VERSION_ID="?([^"\n]+)"?(?:$|\n)', data).group(1)
    entry.architecture = arch
    entry.license = 'free'
    entry.description = re.search(r'(?:^|\n)PRETTY_NAME="?([^"\n]+)"?(?:$|\n)', data).group(1)
    entry.is_managed = True

    logging.info(f'{entry.source} is adding entry {entry.package} to inventory')

    return [entry]


def parse_fields(headers, line):
    last_header = len(headers) - 1
    fields = {}
    pos = 0
    for idx, header in enumerate(headers):
        header_len = len(header)
        value = line[pos:pos + header_len] if idx < last_header else line[pos:]
        fields[header.strip()] = value.strip()
        pos = pos + header_len
    return fields


def json_emitter(hostname: str, results: list):
    with open(f'inventory-{hostname}.json', 'w') as f:
        for r in results:
            json.dump(r, f, default=vars)
            f.write('\n')


def generate_inventory(cmargs):
    arch, err = subprocess.Popen(['uname', '-om'], stdout=PIPE, stderr=PIPE).communicate()
    arch = arch.decode('utf-8').split('\n')[0]

    result = []
    for collector in COLLECTORS:
        # noinspection PyBroadException
        try:
            result += collector(cmargs.hostname, arch)
        except Exception:
            logging.exception('Error while running collector %s: %s, skipping', collector.__name__)
    json_emitter(cmargs.hostname, result)


# main script starts here

COLLECTORS = [
    apt_collector,
    compgen_collector,
    npm_collector,
    snap_collector,
    pip_collector,
    os_collector,
]
parser = argparse.ArgumentParser(description='Generates an inventory of software packages installed on this machine')
parser.add_argument('hostname', type=str,
                    help='The hostname of your machine, typically something like ham-its-1234')
parser.add_argument('--loglevel', type=str, default='INFO', choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
                    help='The log level to use')
args = parser.parse_args()
logging.basicConfig(format='%(levelname)s:%(message)s', level=args.loglevel)
generate_inventory(args)
